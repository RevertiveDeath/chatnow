//  app/models.js

// Get mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

//Define Message schema
var MessageSchema = new Schema({
    chat: { type: ObjectId, ref: 'ChatSchema' },
    sender: { type: ObjectId, ref: 'UserSchema' },
    content: String,
    //Is it a status message
    statusMessage: Boolean,
    // Date of creation
    date: Date,
});

// Define Chat schema
var ChatSchema = new Schema({
    name: String,
    // Date of creation
    date: Date,
    // Date of most recent update
    update: Date,
    // Users
    users: [{ type: ObjectId, ref: 'UserSchema' }]
});

//Define Request schema
var RequestSchema = new Schema({
    type: String, //Friend or Chat request
    //Date requested
    date: Date,
    //Sender of request
    sender: { type: ObjectId, ref: 'UserSchema' },
    //Receiver(s) of request
    //(for friend request, array of one element)
    receivers: [{ type: ObjectId, ref: 'UserSchema' }],
    //Chat id (If applicable)
    chat: { type: ObjectId, ref: 'ChatSchema' }
});

//Define User schema
var UserSchema = new Schema({
    //Main data
    username: String,
    fullname: String,
    joined: Date,
    email: String,
    password: String,
    //Stats
    chats: Number,
    messages: Number,
    //Session token
    token: String,
    //Friends
    friends: [{ type: ObjectId, ref: 'UserSchema' }],
    //Settings
    //Profile color
    color: String,
    //If Audio Notifications are enabled
    audio: Boolean,
});

//Our models to export
var Chat = mongoose.model('Chat', ChatSchema);
var Message = mongoose.model('Message', MessageSchema);
var Request = mongoose.model('Request', RequestSchema);
var User = mongoose.model('User', UserSchema);

//Variable to export
var models = { Chat, Message, Request, User };

module.exports = models;