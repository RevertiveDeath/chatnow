//  app/auth.js
module.exports = {
    authenticate: function(req, models, callback) {
        //Parse url for data-sensitive part
        var request = req.originalUrl;
        if(request.indexOf('/api/') > -1) {
            request = request.replace('/api/', '');
        } else {
            //Not a valid request to our api
            return result(405);
        }
        //Strip off any param or ids
        request = request.split('?')[0];
        request = request.split('/')[0];
        
        //Is the request being handled?
        var handled = false;
        
        //Check if request requires authentication
        if(request === 'global' ||
            request === 'stats' ||
            request === 'login' ||
            request === 'verify' ||
            (request === 'profile' && req.method === 'GET') ||
            request === 'register' && req.method === 'POST') {
            //No authentication required, allow it
            return result(200);
        } else {
            //Authentication required, we now check the auth
            handled = true;
            //Get the user
            var id = req.query.user;
            //Post data in body
            if(id === null || id === undefined) {
                id = req.body.user;
            }
            //Get the token
            var token = req.query.token;
            //Post data in body
            if(token === null || token === undefined) {
                token = req.body.token;
            }
            //No user variable found
            if(id === null || id === undefined) {
                return result(101);
            } 
            //No token supplied
            if(token === null || token === undefined) {
                return result(100);
            } 
            if(request === 'chats' ||
                request === 'search' ||
                request === 'profile' ||
                request === 'register' || //Pass changes
                request === 'request' ||
                request === 'friend' ||
                request === 'messages') {
                //Authenticate token with database
                var query = models.User.findOne({ '_id': id, 'token': token });
                query.exec(function(err, user) {
                    if(err) {
                        //Unknown error
                        callback(result(104));
                    } else if(user == null) {
                        //Failed to authenticate
                        callback(result(105));
                    } else {
                        //Success
                        callback(result(200));
                    }
                });
            } else {
                //Fell through, request not handled after all
                handled = false;
            }
            
        }
        
        
        //Fallback - if no handler, failed
        if(!handled) {
            return result(400, request);
        } else {
            return {
                success: false,
                code: 1,
                message: "Waiting for auth..."
            };
        }
    },
    
    generateToken: function() {
        //Generates 30 character authentication code
        /*  ASCII CODES
            Numbers 0-9: 48->57     - rand 0->9 inclusive
            UpperCase A-Z: 65->90   - rand 10->34 inclusive
            LowerCase a-z: 97->122  - rand 35->59 inclusive
            
            USE: String.fromCharCode(65);
        */
        var token = "";
        var rand;
        for(var i = 0; i < 30; i++) {
            rand = Math.floor((Math.random() * 59) + 1);
            //Adjust for ascii code
            if(rand < 10) {
                //Number
                rand += 48;
            } else if(rand < 35) {
                //Uppercase
                rand += 65 - 10;
            } else {
                rand += 97 - 35;
            }
            token += String.fromCharCode(rand);
        }
        return token;
    }
};

var result = function(code, mod) {
    //Modifier to message (Added at end)
    if(mod === null || mod === undefined) {
        mod = "";
    } else {
        mod = " (" + mod + ")";
    }
    var success = false; //Default to false
    var message = "";
    
    //All auth status codes
    switch(code) {
        case 100:
            message = "NO TOKEN SUPPLIED";
            break;
        case 101:
            message = "NO USER SUPPLIED";
            break;
        case 104:
            message = "AUTHENTICATION ERROR";
            break;
        case 105:
            message = "INVALID TOKEN";
            break;
        case 200:
            success = true;
            message = "OK";
            break;
        case 400:
            message = "BAD REQUEST";
            break;
        case 405:
            message = "BAD API CALL";
            break;
        default:
            message = "ERR - CODE " + code.toString() + " INVALID";
            break;
    }
    
    return {
        success: success,
        code: code,
        message: message + mod
    }
}