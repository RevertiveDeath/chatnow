//  public/js/services/ChatService.js

/* global angular */
angular.module('ChatService', []).factory('Chat', ['$http', function($http) {
    
    return {
        // Call to get all chats
        get : function() {
            return $http.get('/api/chats');
        },
        
        // Call to get a certain chat
        get : function(id) {
            return $http.get('/api/chats/' + id);
        },
        
        // Call to update a certain chat
        update : function(id, chatData) {
            return $http.put('/api/chats/' + id, chatData);
        },
        
        // Call to POST and create a new chat
        create : function(chatData) {
            return $http.post('/api/chats', chatData);
        },
        
        // Call to DELETE a chat
        delete: function(id) {
            return $http.delete('/api/chats/' + id);
        }
    }
    
}]);