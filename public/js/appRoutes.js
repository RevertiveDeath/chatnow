//  public/js/appRoutes.js

/* global angular */
angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    
    // Define the application routes
    $routeProvider
        // Home page
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })
    
        //Login and Register page
        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginController'
        })
        
        //Chat page
        .when('/chats', {
           templateUrl: 'views/chat.html',
           controller: 'ChatController'
        })
        
        //User profile page
        .when('/profile', {
            templateUrl: 'views/profile.html',
            controller: 'ProfileController'
        })
        
        //User settings/edit page
        .when('/settings', {
            templateUrl: 'views/settings.html',
            controller: 'SettingsController'
        })
        
        //Searching for a user
        .when('/search', {
            templateUrl: 'views/search.html',
            controller: 'SearchController'
        })
        
        //Viewing your friends
        .when('/friends', {
            templateUrl: 'views/friend.html',
            controller: 'FriendController'
        })
        
        //Else
        .otherwise({
            redirectTo: '/'
        });
        
    $locationProvider.html5Mode(true);
    
}]);