//  public/js/controllers/ProfileCtrl.js

/* global angular */
angular.module('ProfileCtrl', []).controller('ProfileController', ['$scope', '$location', '$rootScope', '$http', function($scope, $location, $rootScope, $http) {

    //Profile is used to look at ANY profile (including the active user)
    $scope.iscurrentuser = false;
    $scope.isloading = true;
    $scope.friendchats = -1;
    $scope.friendcount = -1;
    $scope.messagecount = -1;
    $scope.chatcount = -1;
    
    //Set value of rootscope var
    $rootScope.editing = false;
    
    //Check if session storage is active
    $rootScope.getData();
    
    //Are we logged in?
    if($rootScope.loggedIn) {
        $scope.user = $rootScope.user;
    } else {
        $location.path('/login');
    }

    //Look for profile query string (And make sure it isn't your own username)
    if($location.search().hasOwnProperty('user') && 
    $location.search()['user'].length > 0 &&
    $location.search()['user'] != $scope.user.username) {
        $scope.friendloading = true;
        Initialize($location.search()['user'], $http, $scope, $rootScope);
    } else {
        $scope.iscurrentuser = true;
        Initialize($scope.user.username, $http, $scope, $rootScope);
    }
    
    //Function bindings
    $scope.Edit = function() {
        $rootScope.editing = true;
        $location.path('/settings');
    }
    
    $scope.Settings = function() {
        $rootScope.editing = false;
        $location.path('/settings');
    }
    
    //Make request to add user as friend
    $scope.Friend = function() {
        //First assume the request is successful
        $scope.friendloading = true;
        
        $http({
            method: 'POST',
            url: '/api/request',
            data: {
                "user": $rootScope.user.id,
                "token": $rootScope.user.token,
                "sender": $rootScope.user.id,
                "type": "friend",
                "receivers": $scope.id
            }
        }).then(function(response) {
            $scope.friendloading = false;
            
            if(response.data.code != 200) {
                if(response.data.code == 400) {
                    //Not successful
                    $scope.requesterror = response.data.message;
                } else {
                    $scope.requesterror = "Could not send request";
                }
            } else {
                $scope.requested = true;
            }
        });
    }
    
    //Accept a received friend request
    $scope.AcceptRequest = function() {
        $scope.friendloading = true;
        
        $http({
            method: 'DELETE',
            url: '/api/request',
            params: {
                "user": $rootScope.user.id,
                "token": $rootScope.user.token,
                "sender": $rootScope.user.id,
                "type": "friend",
                "friend": $scope.id,
                "accept": "true"
            }
        }).then(function(response) {
            $scope.friendloading = false;
            if(response.data.code == 200) {
                //Accepted request
                $scope.requested = false;
                $scope.requestee = false;
                $scope.friends = true;
            } else {
                $scope.requesterror = "Could not accept request";
            }
        });
    }
    
    //Cancel a sent friend request
    $scope.CancelRequest = function() {
        $scope.friendloading = true;
        
        $http({
            method: 'DELETE',
            url: '/api/request',
            params: {
                "user": $rootScope.user.id,
                "token": $rootScope.user.token,
                "sender": $rootScope.user.id,
                "type": "friend",
                "friend": $scope.id
            }
        }).then(function(response) {
            $scope.friendloading = false;
            if(response.data.code == 200) {
                //Deleted request
                $scope.requested = false;
            } else {
                $scope.requesterror = "Could not delete request";
            }
        });
    }
    
    //Cance; a received friend request
    $scope.DeleteRequest = function() {
        $scope.friendloading = true;
        
        $http({
            method: 'DELETE',
            url: '/api/request',
            params: {
                "user": $rootScope.user.id,
                "token": $rootScope.user.token,
                "sender": $scope.id,
                "type": "friend",
                "friend": $rootScope.user.id
            }
        }).then(function(response) {
            $scope.friendloading = false;
            if(response.data.code == 200) {
                //Deleted request
                $scope.requestee = false;
            } else {
                $scope.requesterror = "Could not delete request";
            }
        });
    }
    
    //Remove a user from your friends
    $scope.Unfriend = function() {
        $scope.friendloading = true;
        
        $http({
            method: 'DELETE',
            url: '/api/friend',
            params: {
                "user": $rootScope.user.id,
                "token": $rootScope.user.token,
                "friend": $scope.id
            }
        }).then(function(response) {
            $scope.friendloading = false;
            if(response.data.code == 200) {
                $scope.friends = false;
            } 
        });
    }
}]);

var Initialize = function(username, http, scope, rootScope) {
    //Initial default data
    scope.username = username;
    scope.fullname = false;
    scope.color = "#E0E0E0";
    scope.exists = false;
    
    LoadProfile(username, http, scope, rootScope);
}

var LoadProfile = function(username, http, scope, rootScope) {
    //Queries the server to see if this user's profile is set up
    http({
        method: 'GET',
        url: '/api/profile',
        params: {
            "user": username
        }
    }).then(function(response) {
        scope.chatcount = -2;
        scope.messagecount = -2;
        scope.friendcount = -2;
        if(response.data.code == 200) {
            var user = response.data.user;
            scope.exists = true;
            scope.id = user.id;
            scope.joined = user.joined;
            if(user.chats !== null && user.chats !== undefined) {
                scope.chatcount = user.chats;
            } else {
                scope.chatcount = 0;
            }
            if(user.messages !== null && user.messages !== undefined) {
                scope.messagecount = user.messages;
            } else {
                scope.messagecount = 0;
            }
            if(!(user.fullname == undefined || 
                user.fullname == null || 
                user.fullname.length == 0)) {
                    scope.fullname = user.fullname;
                }
            if(!(user.color == undefined ||
                user.color == null ||
                user.color.length == 0)) {
                scope.color = user.color;
            }

            if(user.friends !== null && user.friends !== undefined && user.friends.length !== null && user.friends.length !== undefined) {
                if(!scope.iscurrentuser && user.friends.indexOf(rootScope.user.id) > -1) {
                    //We are friends
                    scope.friends = true;
                }
                scope.friendcount = user.friends.length;
            }
        } else if(response.data.code == 404) {
            //User not found
            scope.error = 'User "' + username + '" not found!';
        } else {
            scope.error = 'An error has occurred';
        }

        //Commit data to style
        scope.style = {
            'background-color': scope.color
        };
        
        //Next two requests only apply if not current user
        if(!scope.iscurrentuser) {
            LoadFriendship(username, http, scope, rootScope);
            LoadFriendshipChats(http, scope, rootScope);
        }
    });
}

var LoadFriendshipChats = function(http, scope, rootScope) {
    //Load chats for current user and check if any contain this user
    http({
        method: 'GET',
        url: '/api/chats',
        params: {
            "user": rootScope.user.id,
            "token": rootScope.user.token
        }
    }).then(function(response) {
        scope.friendchats = -2;
        if(response.data.code == 200) {
            var chats = response.data.chats;
            if(chats !== null && chats !== undefined && chats.length !== null && chats.length !== undefined) {
                scope.friendchats = 0;
                for(var i = 0; i < chats.length; i++) {
                    if(chats[i].users !== null && chats[i].users !== undefined && chats[i].users.length !== null && chats[i].users.length !== undefined) {
                        var index = chats[i].users.indexOf(scope.id);
                        if(index > -1) {
                            scope.friendchats++;
                        }
                    }
                }
            }
        }
    });
}

var LoadFriendship = function(username, http, scope, rootScope) {
    //Defaults to false
    scope.friends = false;
    scope.request = false;
    //Queries server to see if you are friends
    http({
        method: 'GET',
        url: '/api/friend',
        params: {
            "user": rootScope.user.id,
            "token": rootScope.user.token,
            "friend": scope.id
        }
    }).then(function(response) {
        if(response.data.code == 200) {
            if(response.data.friends == false) {
                //Queries server to see if they have sent us a friend request
                http({
                    method: 'GET',
                    url: '/api/request',
                    params: {
                        "user": rootScope.user.id,
                        "token": rootScope.user.token,
                        "sender": scope.id,
                        "type": "friend",
                        "friend": rootScope.user.id
                    }
                }).then(function(response) {
                    if(response.data.code == 200) {
                        if(response.data.requested) {
                            scope.friendloading = false;
                            scope.requestee = true;
                        } else {
                            //Queries server to see if we have sent a friend request
                            http({
                                method: 'GET',
                                url: '/api/request',
                                params: {
                                    "user": rootScope.user.id,
                                    "token": rootScope.user.token,
                                    "sender": rootScope.user.id,
                                    "type": "friend",
                                    "friend": scope.id
                                }
                            }).then(function(response) {
                                scope.friendloading = false;
                                if(response.data.code == 200) {
                                    if(response.data.requested) {
                                        scope.requested = true;
                                    }
                                } else if(response.data.code == 400) {
                                    scope.requesterror = "Failed to check if friendship requested";
                                }
                            });
                        }
                    } else if(response.data.code == 400) {
                        scope.requesterror = "Failed to check if friendship requested";
                    }
                });
            } else {
                scope.friends = true;
                scope.friendloading = false;
            }
        } else {
            scope.frienderror = "Failed to check friendship";
            scope.friendloading = false;
        }
    });
}