//  public/js/controllers/FriendCtrl.js

/* global angular */
angular.module('FriendCtrl', []).controller('FriendController', ['$scope', '$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location) {
    $scope.friends= null;
    $scope.isloading = true;
    
    //Check if session storage is active
    $rootScope.getData();
    
    //Are we logged in?
    if($rootScope.loggedIn) {
        $scope.user = $rootScope.user;
    } else {
        $location.path('/login');
    }
    
    //Load friend list
    $scope.friendids = [];
    $http({
        method: 'GET',
        url: '/api/profile',
        params: {
            "id": $scope.user.id
        }
    }).then(function(response) {
        if(response.data.code == 200) {
            $scope.friendids = response.data.user.friends;
            
            if($scope.friendids == undefined || $scope.friendids == null || $scope.friendids.length == 0) {
                $scope.isloading = false;
                $scope.friends = null;
            } else {
                //Load friends
                $scope.friends = [];
                for(var i = 0; i < $scope.friendids.length; i++) {
                    LoadFriend($scope, $http, i);
                }
            }
        } else {
            $scope.isloading = false;
            $scope.friends = null;
        }
    });  
    
}]);

var LoadFriend = function(scope, http, index) {
    //Loads a friend's data from the server
    http({
        method: 'GET',
        url: '/api/profile',
        params: {
            "id": scope.friendids[index]
        }
    }).then(function(response) {
        if(response.data.code == 200) {
            scope.friends.push(response.data.user);
        } else {
            scope.friends.push({ error: true });
        }
        
        if(scope.friends.length == scope.friendids.length) {
            scope.friends.sort(function(x, y) {
                var xcomp = x.username;
                if(x.fullname !== null && x.fullname !== undefined && x.fullname.length > 0) {
                    xcomp = x.fullname
                }
                
                var ycomp = y.username;
                if(y.fullname !== null && y.fullname !== undefined &&  y.fullname.length > 0) {
                    ycomp = y.fullname
                }
                
                return xcomp > ycomp;
            });
            
            scope.isloading = false;
        }
    });
}