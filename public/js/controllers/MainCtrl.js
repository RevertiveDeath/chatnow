//  public/js/controllers/MainCtrl.js

/* global angular */
/* global moment */
angular.module('MainCtrl', []).controller('MainController', ['$scope', '$rootScope', '$location', '$http', '$interval', function($scope, $rootScope, $location, $http, $interval) {
    
    //Load initial stat data
    GetStats($scope, $http);
    
    //Timer updates values once a minute
    var tmr = $interval(function() {
        GetStats($scope, $http);
    }, 5000);
    
    //Set up timer clearing
    $scope.$on("$destroy", function(){
        $interval.cancel(tmr);
    });
    
    //Check if session storage is active
    $rootScope.getData();
}]);

function GetStats(scope, http) {
    http({
        method: 'GET',
        url: '/api/global'
    }).then(function(response) {
        var chats = response.data.chats;
        var messages = response.data.messages;
        var users = response.data.users;
        if(chats !== null && chats !== undefined) {
            scope.totalChats = chats;
        } else {
            scope.totalChats = "error";
        }
        if(messages !== null && messages !== undefined) {
            scope.totalMessages = messages;
        } else {
            scope.totalMessages = "error";
        }
        if(users !== null && users !== undefined) {
            scope.totalUsers = users;
        } else {
            scope.totalUsers = "error";
        }
        scope.version = response.data.version;
    });
    scope.updated = moment().format('h:mm a - MMM DD, YYYY');
}