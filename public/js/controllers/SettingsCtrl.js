//  public/js/controllers/SettingsCtrl.js

/* global angular */
angular.module('SettingsCtrl', []).controller('SettingsController', ['$scope', '$location', '$timeout', '$localStorage', '$rootScope', '$http', function($scope, $location, $timeout, $localStorage, $rootScope, $http) {
    //Check if session storage is active
    $rootScope.getData();
    
    //Are we logged in?
    if($rootScope.loggedIn) {
        $scope.user = $rootScope.user;
    } else {
        $location.path('/login');
    }
    
    //Check if we were editing
    if($rootScope.editing === undefined || $rootScope.editing === null) {
        $rootScope.editing = $localStorage.editing;
        if($rootScope.editing === undefined || $rootScope.editing === null) {
            $rootScope.editing = false;
        }
    }
    //Update local storage
    $localStorage.editing = $rootScope.editing;
    
    //Load profile data
    $scope.loading = true;
    
    $scope.toggleAudio = function() {
        //We will visually toggle first, then send the request
        $scope.audioNotify = !$scope.audioNotify;
        
        $http({
            method: 'POST',
            url: '/api/profile',
            data: {
                "user": $rootScope.user.id,
                "token": $rootScope.user.token,
                "fullname": null,
                "color": null,
                "audio": $scope.audioNotify
            }
        }).then(function(response) {
            if(response.data.code == 200) {
                $scope.audioNotify = response.data.audio;
            }
            $rootScope.reloadData();
        });
    }
    
        $scope.DeleteProfile = function(scope, rootScope, http, location) {
        http({
            method: 'DELETE',
            url: '/api/profile',
            params: {
                "user": rootScope.user.id,
                "token": rootScope.user.token
            }
        }).then(function(response) {
            if(response.data.code == 200) {
                //Deleted
                rootScope.logout();
                location.path('/');
            } else {
                //Error stored at response.data.message
                alert(response.data.message);
                location.path('/profile');
            }
        });
    }
    
    $scope.LoadProfile = function(username, http, scope, timeout) {
        //Initial default data
        scope.username = username;
        scope.fullname = null;
        scope.color = null;
        scope.exists = false;
        //Queries the server to see if this user's profile is set up
        http({
            method: 'GET',
            url: '/api/profile',
            params: {
                "user": username
            }
        }).then(function(response) {
            if(response.data.code == 200) {
                var user = response.data.user;
                scope.exists = true;
                scope.joined = user.joined;
                if(!(user.fullname === undefined || 
                    user.fullname === null || 
                    user.fullname.length === 0)) {
                        scope.fullname = user.fullname;
                    }
                if(!(user.color == undefined ||
                    user.color == null ||
                    user.color.length == 0)) {
                    scope.color = user.color;
                }
            } else if(response.data.code == 404) {
                //User not found
                scope.error = 'User "' + username + '" not found!';
            } else {
                scope.error = 'An error has occurred';
            }
    
            //Commit data to style
            scope.style = {
                'background-color': scope.color
            };
            
            //Done loading
            $scope.loadTimer = timeout(function() {scope.loading = false;}, 500);
        });
    }
    
    $scope.Hash = function(data) {
        var hash = 0, i, chr, len;
        
        for (i = 0, len = data.length; i < len; i++) {
            chr   = data.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }
    
    $scope.audioNotify = $scope.user.audio;
    if($scope.audioNotify == null || $scope.audioNotify == undefined) {
        $scope.audioNotify = false;
        $scope.toggleAudio();
    }
    
    $scope.LoadProfile($scope.user.username, $http, $scope, $timeout);
    
    //Function bindings
    $scope.SubmitData = function() {
        var scope = $scope;
        if(scope.fullname != null) {
            scope.fullname = scope.fullname.trim();
        }
        if(scope.color != null) {
            scope.color = scope.color.trim();
        }
        
        //Updates on the server
        $http({
            method: 'POST',
            url: '/api/profile',
            data: {
                "user": $rootScope.user.id,
                "token": $rootScope.user.token,
                "fullname": scope.fullname,
                "color": scope.color
            }
        }).then(function(response) {
            if(response.data.code == 200) {
                $location.path('/profile');
            } else {
                
            }
        });
    }
    
    //Change password
    $scope.ChangePassword = function() {
        //We are in loading state
        $scope.loading = true;
        //Reset password error
        $scope.passworderror = null;
        //Set initial progress info
        $scope.progress = "Validating input fields";
        
        //First validate the input fields
        if($scope.oldpass == undefined || $scope.oldpass.trim().length == 0) {
            $scope.passworderror = "Please input old password";
        } else if($scope.pass == undefined || $scope.pass.trim().length == 0) {
            $scope.passworderror = "Password cannot be empty";
        } else if($scope.pass.trim().length < 8) {
            $scope.passworderror = "Password must be at least 8 characters long";
        } else if($scope.passconfirm == undefined || $scope.pass.trim() != $scope.passconfirm.trim()) {
            $scope.passworderror = "Passwords do not match";
        } else if($scope.oldpass.trim() == $scope.pass.trim()) {
            $scope.passworderror = "Old password must not match new one";  
        }
        
        //If clientside validation failed, exit the function without bothering the server
        if($scope.passworderror !== null) {
            $scope.loading = false;
            return;
        }
        
        //Perform the change
        $scope.progress = "Waiting for server";
        
        $http({
            method: 'PUT',
            url: '/api/register',
            data: {
                "user": $scope.user.id,
                "token": $scope.user.token,
                "oldpass": $scope.Hash($scope.oldpass.trim()),
                "pass": $scope.Hash($scope.pass.trim())
            }
        }).then(function(response) {
            var data = response.data;
            $scope.loading = false;
            if(data.code == 200) {
                //Success, redirect back
                $scope.updated = true;
            } else if(data.code == 402) {
                //Incorrect old password
                $scope.passworderror = "Incorrect old password";
            } else {
                //Unknown error
                $scope.error = true;  
            }
        });
    }
    
    //Delete account
    $scope.Delete = function() {
        $scope.deleting = true;
        $scope.deleteTimer = $timeout(function() { $scope.DeleteProfile($scope, $rootScope, $http, $location) }, 2000);
    }
    
    //Clear scope timers
    $scope.$on(
        "$destroy",
        function( event ) {
            $timeout.cancel( $scope.deleteTimer );
            $timeout.cancel( $scope.loadTimer );
        }
    );
}]);