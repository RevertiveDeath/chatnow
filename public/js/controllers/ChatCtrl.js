//  public/js/controllers/ChatCtrl.js

/* global angular */
angular.module('ChatCtrl', []).controller('ChatController', ['$scope', '$timeout', '$rootScope', '$http', '$location', function($scope, $timeout, $rootScope, $http, $location) {

    //Check if session storage is active
    $rootScope.getData();
    
    if($rootScope.loggedIn) {
        $scope.user = $rootScope.user;
    } else {
        $location.path('/login');
    }
    
    // Set the name of the hidden property and the change event for visibility
    var hidden, visibilityChange; 
    if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
      hidden = "hidden";
      visibilityChange = "visibilitychange";
    } else if (typeof document.mozHidden !== "undefined") {
      hidden = "mozHidden";
      visibilityChange = "mozvisibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
      hidden = "msHidden";
      visibilityChange = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
      hidden = "webkitHidden";
      visibilityChange = "webkitvisibilitychange";
    }
    
    function handleVisibilityChange() {
      if (document[hidden]) {
        $scope.visible = false;
      } else {
        $scope.visible = true;
        document.title = "Chat Now";
      }
    }
    
    // Warn if the browser doesn't support addEventListener or the Page Visibility API
    if (!(typeof document.addEventListener === "undefined" || typeof document[hidden] === "undefined")) {
        // Handle page visibility change   
        document.addEventListener(visibilityChange, handleVisibilityChange, false);
        window.addEventListener(visibilityChange, handleVisibilityChange, false);
        
        // extra event listeners for better behaviour
        document.addEventListener('focus', function() {
          handleVisibilityChange(true);
        }, false);
        
        document.addEventListener('blur', function() {
          handleVisibilityChange(false);
        }, false);
        
        window.addEventListener('focus', function() {
            handleVisibilityChange(true);
        }, false);
        
        window.addEventListener('blur', function() {
          handleVisibilityChange(false);
        }, false);
    }
    
    $scope.friendsloading = true;
    $scope.friends = null;
    $scope.selected = 0;
    $scope.maxselected = 5;
    $scope.chatMargin = 1000;
    $scope.flash = false;
    $scope.newchat = {};
    $scope.newchaterror = null;
    $scope.chaterror = null;
    $scope.friendids = [];
    $scope.chat = null;
    $scope.sendingMessage = false;
    $scope.chatwindow = document.getElementById('chatwindow');
    $scope.chatinput = document.getElementById('chatinput');
    $scope.audio = {
        newmessage: document.getElementById('snd_newmessage')
    };
    
    //Create scope functions
    CreateChatFunctions($scope, $http, $timeout, $location);
    $scope.randomSubject();
    
    //We display extra header info if we just logged back in
    if($scope.redir == true || $rootScope.redirect == true) {
        //We were redirected from a login
        $scope.redir = true;
        $rootScope.redirect = undefined;
        //Check if user's profile has been setup yet
        //First assume it is, request may take a while
        $scope.setup = true;
        $scope.UserSetup($scope, $http);
    } else {
        $scope.redir = false;
    }
    
    //Function to set up new profile
    $scope.SetUp = function() {
        $rootScope.editing = true;
        $location.path('/settings');
    }
    
    //Load friends for chat use
    $scope.LoadFriends($scope, $http, $timeout);
    
    //Load chats
    $scope.Sync($scope, $rootScope, $http, $timeout, true);
    
    $scope.$on(
        "$destroy",
        function( event ) {
            $timeout.cancel( $scope.syncTimer );
            $timeout.cancel( $scope.flashTimer );
            $timeout.cancel( $scope.scrollTimer );
        }
    );
}]);

var CreateChatFunctions = function(scope, http, timeout, location) {
    //Creates functions the scope needs to call from html
    scope.selectFriend = function(friend) {
        if(!friend.selected && scope.selected < scope.maxselected) {
            scope.selected++;
            friend.selected = true;
        } else if(friend.selected) {
            scope.selected--;
            friend.selected = false;
        } else {
            //Flash max friends warning
            scope.flash = true;
            scope.flashTimer = timeout(function() {
                scope.flash = false;
            }, 2000); //After 2 seconds the flash fades out
        }
    };
    
    scope.randomSubject = function() {
        var rand = Math.floor(Math.random() * 5);
        if(rand == 0) {
            scope.newchat.subject = "Gym buddies";
        } else if(rand == 1) {
            scope.newchat.subject = "Friday night party buddies";
        } else if(rand == 2) {
            scope.newchat.subject = "Surprise party ideas";
        } else if(rand == 3) {
            scope.newchat.subject = "School homework sharing";
        } else if(rand == 4) {
            scope.newchat.subject = "Freshmen from 2013";
        }
    }
    
    scope.createChat = function() {
        //Who is chatting in the new chat
        var users = [];
        users.push(scope.user.id);
        for(var i = 0; i < scope.friends.length; i++) {
            if(scope.friends[i].selected) {
                users.push(scope.friends[i].id);
            }
            scope.friends[i].selected = false;
        }
        scope.selected = 0;
        //The name/subject of the new chat
        var name = scope.newchat.subject
        //Create the chat
        http({
            method: 'POST',
            url: '/api/chats',
            data: {
                "user": scope.user.id,
                "token": scope.user.token,
                "users": users,
                "name": name
            }
        }).then(function(response) {
            if(response.data.code != 200) {
                scope.newchaterror = response.data.message;
            } else {
                //Reset new chat window
                for(var i = 0; i < scope.friends.length; i++) {
                    scope.friends[i].selected = false;
                }
                
                scope.chat = response.data.chat;
                scope.LoadChats(scope, http, timeout);
            }
        });
    }
    
    scope.chatClick = function(chat) {
        //Logic based on selected chat
        scope.visible = true;
        if(chat.selected) {
            scope.chat = null;
        } else {
            scope.chat = chat;
        }
        //Deselect all chats
        for(var i = 0; i < scope.chats.length; i++) {
            if(scope.chats[i] != chat) {
                scope.chats[i].selected = false;
            }
        }
        //Select this chat
        chat.selected = !chat.selected;
        //If we are viewing a chat, load its messages
        if(chat.selected) {
            chat.newmessage = false;
            document.title = "Chat Now";
            scope.LoadMessages(scope, http, timeout, true);
        }
    }
    
    scope.viewUser = function(user) {
        location.path('/profile').search({user: user.username});
    }
    
    scope.deleteChat = function(chat) {
        scope.chatToDelete = chat;
    }
    
    scope.confirmDeleteChat = function() {
        var chat = scope.chatToDelete;
        if(chat !== null && chat !== undefined) {
            //Send message to remove self from chat

            http({
                method: 'POST',
                url: '/api/messages/' + chat._id,
                data: {
                    "user": scope.user.id,
                    "token": scope.user.token,
                    "content": "has left the chat",
                    "statusMessage": true
                }
            }).then(function(response) {
                //Delete chat
                http({
                    method: 'DELETE',
                    url: '/api/chats/' + chat._id,
                    params: {
                        "user": scope.user.id,
                        "token": scope.user.token
                    }
                }).then(function(response) {
                    if(response.data.code == 200) {
                        //Remove chat from our view
                        var index = scope.chats.indexOf(chat);
                        if(index > -1) {
                            scope.chats.splice(index, 1);
                        }
                        if(scope.chats.length == 0) {
                            scope.chats = [];
                            scope.chat = null;
                        } else {
                            scope.chat = scope.chats[0];
                            scope.chat.selected = true;
                            scope.LoadMessages(scope, http, timeout, true);
                        }
                    }
                });
            });
        }
    }
    
    scope.sendMessage = function() {
        scope.visible = true;
        document.title = "Chat Now";
        
        if(scope.sendingMessage) {
            return;
        }
        var chat = scope.chat;
        var msg = scope.newmessage;
        if(chat == null || chat == undefined || msg == null || msg == undefined || msg.content == null || msg.content == undefined) {
            return;
        }
        if(msg.content.length > 300) {
            msg.content = msg.content.substr(0,300);
        }

        scope.sendingMessage = true;

        http({
            method: 'POST',
            url: '/api/messages/' + chat._id,
            data: {
                "user": scope.user.id,
                "token": scope.user.token,
                "content": msg.content
            }
        }).then(function(response) {
            scope.sendingMessage = false;
            scope.chatinput.focus();
            if(response.data.code == 200) {
                scope.newmessage.content = "";
                var message = response.data.msg;
                for(var i = 0; i < chat.users.length; i++) {
                    if(chat.users[i].id == message.sender) {
                        message.sender = chat.users[i];
                        break;
                    }
                }
                if(chat.messages == null || chat.messages == undefined) {
                    chat.messages = [];
                }
                chat.messages.push(message);
                scope.scrollTimer = timeout(function() {
                    scope.chatwindow.scrollTop = scope.chatwindow.scrollHeight;
                }, 50);
            } else {
                //TODO handle error
            }
        });
    }
    
    scope.Sync = function(scope, rootScope, http, timeout, firstload) {
        scope.user.audio = rootScope.user.audio;
        scope.syncTimer = timeout(function() {
            scope.Sync(scope, rootScope, http, timeout, false);
        }, 3000);
        
        scope.LoadChats(scope, http, timeout);
        scope.PopulateChat(scope, http, timeout);
        scope.LoadMessages(scope, http, timeout, firstload);
    }
    
    scope.LoadChats = function(scope, http, timeout) {
        var selectedChat = null;
        if(scope.chat !== null && scope.chat !== undefined) {
            selectedChat = scope.chat._id;
        }
        
        http({
            method: 'GET',
            url: '/api/chats',
            params: {
                "user": scope.user.id,
                "token": scope.user.token
            }
        }).then(function(response) {
            if(response.data.code != 200) {
                scope.chaterror = "An error occurred while retrieving chats. Please try again later.";
            } else {
                var chats = response.data.chats;
                var copy = false;
                if(chats !== null && chats !== undefined && chats.length !== null && chats.length !== undefined &&
                    scope.chats !== null && scope.chats !== undefined && scope.chats.length !== null && scope.chats.length !== undefined) {
                    if(scope.chats.length !== chats.length) {
                        copy = true;
                    } else {
                        scope.beep = false;
                        for(var i = 0; i < chats.length; i++) {
                            for(var j = 0; j < scope.chats.length; j++) {
                                if(chats[i]._id == scope.chats[j]._id) {
                                    if(scope.chats[j].users.length != chats[i].users.length) {
                                        scope.chats[j].users = chats[i].users;
                                    }
                                    if(scope.chats[j].newmessage) {
                                        chats[i].newmessage = true;
                                    }
                                    
                                    if(i != j) {
                                        copy = true;
                                    }
                                    
                                    //If the retrieved chat data is more recent than the local copy
                                    if(chats[i].update > scope.chats[j].update) {
                                        //Grab the new timestamp from the backend
                                        scope.chats[j].update = chats[i].update;
                                        //If we are in another tab or another application, alert the user
                                        if(!scope.visible) {
                                            //Change the title
                                            document.title = "! New Message !";
                                            //If we have not beeped yet and audio notify is enabled
                                            if(!scope.beep && scope.user.audio) {
                                                //Alert the user with a beep
                                                scope.audio.newmessage.play();
                                                scope.beep = true;
                                            }
                                            //If the chat is not already selected, we put !New Message! under the updated chat
                                            if(chats[i]._id != selectedChat) {
                                                chats[i].newmessage = true;
                                                scope.chats[j].newmessage = true;
                                            }
                                        }
                                    }
                                } else if(i == j) {
                                    copy = true;
                                }
                            }
                        }
                    }
                } else {
                    copy = true;
                }
                
                //Copy all chat data
                if(copy) {
                    scope.chats = chats;
                }
                
                //Update the selected chat from the new chat data
                if(selectedChat != null && selectedChat != undefined) {
                    for(var i = 0; i < scope.chats.length; i++) {
                        if(scope.chats[i]._id == selectedChat) {
                            scope.chat = scope.chats[i];
                            scope.chat.selected = true;
                            break;
                        }   
                    }
                }
                //Check if friends are loaded, then populate chat objects
                if(!scope.friendsloading) {
                    scope.PopulateChat(scope, http, timeout);
                }
            }
        });
    }
    
    scope.LoadMessages = function(scope, http, timeout, firstload) {
        if(scope.chat == null || scope.chat == undefined || scope.chat._id == null || scope.chat._id == undefined) {
            return;
        }
        
        http({
            method: 'GET',
            url: '/api/messages/' + scope.chat._id,
            params: {
                "user": scope.user.id,
                "token": scope.user.token
            }
        }).then(function(response) {
            if(response.data.code == 200) {
                var messages = response.data.messages;
                messages.reverse();
                var chat = scope.chat;
                var scroll = false;
                if(chat !== null && chat !== undefined && 
                    messages !== null && messages !== undefined && 
                    messages.length !== null && messages.length !== undefined) {
                    var load = false;
                    if(chat.messages !== null && chat.messages !== undefined && 
                        chat.messages.length !== null && chat.messages.length !== undefined &&
                        chat.messages[chat.messages.length - 1] !== null && chat.messages[chat.messages.length - 1] !== undefined &&
                        messages[messages.length - 1] !== null && messages[messages.length - 1] !== undefined) {
                        if(chat.messages[chat.messages.length - 1].date != messages[messages.length - 1].date) {
                            //If true, we need to scroll
                            if(scope.chatwindow.scrollTop == 0 && scope.chatwindow.scrollHeight == scope.chatwindow.offsetHeight) {
                                scroll = true;
                            } else {
                                scroll = (scope.chatwindow.scrollTop > scope.chatwindow.scrollHeight - scope.chatMargin);
                            }
                            
                            chat.messages = [];
                            load = true;
                        }
                    } else {
                        firstload = true;
                        chat.messages = [];
                        load = true;
                    }
                    if(load) {
                        for(var i = 0; i < messages.length; i++) {
                            
                            var message = messages[i];
                            for(var j = 0; j < chat.users.length; j++) {
                                if(chat.users[j].id == message.sender) {
                                    message.sender = chat.users[j];
                                    break;
                                }
                            }
                            
                            //Did we actually get the data?
                            if (typeof message.sender === 'string' || message.sender instanceof String) {
                                //No, still an ID, put placeholder username while real name loads
                                var id = message.sender;
                                message.sender = {
                                    'id': id,
                                    'loading': true
                                };
                                chat.messages.push(message);
                                scope.GetSender(scope, http, chat.messages.length - 1);
                            } else {
                                chat.messages.push(message);
                            }
                        }
                    }
                }
                if(scroll || firstload) {
                    scope.scrollTimer = timeout(function() {
                        scope.chatwindow.scrollTop = scope.chatwindow.scrollHeight;
                    }, 50);
                }
            }
        });
    }
    
    scope.GetSender = function(scope, http, messagenum) {
        http({
            method: 'GET',
            url: '/api/profile/',
            params: {
                "id": scope.chat.messages[messagenum].sender.id,
            }
        }).then(function(response) {
            if(response.data.code == 200) {
                scope.chat.messages[messagenum].sender = {
                    "username": response.data.user.username,
                    "fullname": response.data.user.fullname
                };
            }
        });
    }
    
    scope.PopulateChat = function(scope, http, timeout) {
        if(!(scope.chats !== null && scope.chats !== undefined &&
            scope.chats.length !== null && scope.chats.length !== undefined)) {
            return;
        }
        for(var i = 0; i < scope.chats.length; i++) {
            //We now load the friend data for each chat if we don't already have it
            for(var j = 0; j < scope.chats[i].users.length; j++) {
                if(scope.friends !== null && scope.friends !== undefined) {
                    for(var k = 0; k < scope.friends.length; k++) {
                        if(scope.chats[i].users[j] === scope.friends[k].id) {
                            scope.chats[i].users[j] = scope.friends[k];
                        }
                    }
                }
                if(scope.chats[i].users[j] === scope.user.id) {
                    scope.chats[i].users[j] = scope.user;
                }
                if (scope.chats[i].users[j].id === null || scope.chats[i].users[j].id === undefined) {
                    scope.LoadUser(scope, http, i, j)
                }
            }

        }
    }
    
    scope.LoadUser = function(scope, http, chatindex, userindex) {
        var id = scope.chats[chatindex].users[userindex];
        scope.chats[chatindex].users[userindex] = { loading: true };
        http({
            method: 'GET',
            url: '/api/profile',
            params: {
                "id": id
            }
        }).then(function(response) {
            if(response.data.code == 200) {
                var user = response.data.user;
                if(user.fullname !== null && user.fullname !== undefined && user.fullname.length > 0) {
                    //We use first letter of full name
                    user.letter = user.fullname.charAt(0);
                } else {
                    user.letter = user.username.charAt(0);
                }
                scope.chats[chatindex].users[userindex] = user;
            } else {
                //Error loading user
            }
        });
    }
    
    scope.LoadFriends = function(scope, http, timeout) {
        http({
            method: 'GET',
            url: '/api/profile',
            params: {
                "id": scope.user.id
            }
        }).then(function(response) {
            if(response.data.code == 200) {
                scope.friendids = response.data.user.friends;
                if(scope.friendids == undefined || scope.friendids == null || scope.friendids.length == 0) {
                    scope.friendsloading = false;
                    scope.friends = null;
                } else {
                    //Load friends
                    scope.friends = [];
                    for(var i = 0; i < scope.friendids.length; i++) {
                        scope.LoadFriend(scope, http, i, timeout);
                    }
                }
            } else {
                scope.friendsloading = false;
                scope.friends = null;
            }
        });
    }
    
    
    scope.LoadFriend = function(scope, http, index, timeout) {
        //Loads a friend's data from the server
        http({
            method: 'GET',
            url: '/api/profile',
            params: {
                "id": scope.friendids[index]
            }
        }).then(function(response) {
            if(response.data.code == 200) {
                var friend = response.data.user;
                if(friend.fullname !== null && friend.fullname !== undefined && friend.fullname.length > 0) {
                    //We use first letter of full name
                    friend.letter = friend.fullname.charAt(0);
                } else {
                    friend.letter = friend.username.charAt(0);
                }
                friend.selected = false;
                scope.friends.push(friend);
            } else {
                scope.friends.push({ error: true });
            }
            
            if(scope.friends.length == scope.friendids.length) {
                scope.friends.sort(function(x, y) {
                    var xcomp = x.username;
                    if(x.fullname !== null && x.fullname !== undefined && x.fullname.length > 0) {
                        xcomp = x.fullname
                    }
                    
                    var ycomp = y.username;
                    if(y.fullname !== null && y.fullname !== undefined &&  y.fullname.length > 0) {
                        ycomp = y.fullname
                    }
                    
                    return xcomp > ycomp;
                });
                scope.friendsloading = false;
                if(scope.maxselected > scope.friends.length) {
                    scope.maxselected = scope.friends.length;
                }
                //Check if chats are loaded, then populate chat objects
                if(scope.chats !== null && scope.chats !== undefined && scope.chats.length > 0) {
                    scope.PopulateChat(scope, http, timeout);
                }
            }
        });
    }
    
    scope.UserSetup = function(scope, http) {
        //Queries the server to see if this user's profile is set up
        http({
            method: 'GET',
            url: '/api/profile',
            params: {
                "user": scope.user.username,
            }
        }).then(function(response) {
            //OK response
            if(response.data.code == 200) {
                var user = response.data.user;
                var token = scope.user.token;
                scope.user = user;
                scope.user.token = token;
                if(user.fullname == undefined || 
                    user.fullname == null || 
                    user.fullname == "" ||
                    user.color == undefined ||
                    user.color == null ||
                    user.color == "") {
                    //Data is not setup yet
                    scope.setup = false;
                }
            }
        });
    }
}