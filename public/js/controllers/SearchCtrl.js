//  public/js/controllers/SearchCtrl.js

/* global angular */
angular.module('SearchCtrl', []).controller('SearchController', ['$scope', '$location', '$rootScope', '$http', function($scope, $location, $rootScope, $http) {
    
    //Check if session storage is active
    $rootScope.getData();
    
    //Are we logged in?
    if($rootScope.loggedIn) {
        $scope.user = $rootScope.user;
    } else {
        $location.path('/login');
    }
    
    //Look for search query string
    if($location.search().hasOwnProperty('param') && 
    $location.search()['param'].length > 0 &&
    $location.search()['param'] != $scope.user.username) {
        var trimmed = $location.search()['param'];
        if(trimmed.length > 40) {
            trimmed = trimmed.substr(0, 40);
        }
        $scope.fullparam = trimmed;
        var params = trimmed.split(" ");
        $scope.searchparams = params;
        $scope.noparam = false;
        $scope.loading = true;
        FetchUsers($scope, $http, params);
    } else {
        $scope.noparam = true;
    }
    
    //Click on result
    $scope.ViewProfile = function(username) {
        $location.path('/profile').search({user: username});;
    }
}]);

var FetchUsers = function(scope, http, params) {
    //Queries the server to get results
    http({
        method: 'GET',
        url: '/api/search',
        params: {
            "user": scope.user.id,
            "token": scope.user.token,
            "params": scope.searchparams
        }
    }).then(function(response) {
        var code = response.data.code;
        if(code == 200) {
            //Data stored in response.data.results
            var results = response.data.results;
            //If user has not set up color yet, default to grey
            for(var i = 0; i < results.length; i++) {
                if(results[i].color == null || 
                    results[i].color == undefined || 
                    results[i].color.length == 0) {
                    results[i].color = "Grey";
                }
            }
            results.sort(function(x, y) {
                var xcomp = x.username;
                if(x.fullname !== null && x.fullname !== undefined && x.fullname.length > 0) {
                    xcomp = x.fullname
                }
                
                var ycomp = y.username;
                if(y.fullname !== null && y.fullname !== undefined &&  y.fullname.length > 0) {
                    ycomp = y.fullname
                }
                
                return xcomp > ycomp;
            });
            //Store in scope
            scope.results = results;
        } else {
            //Error
            if(code == 404) {
                scope.error = "Sorry, no matches found. Try simplifying your search!";
            } else if(code == 400) {
                scope.error = "An unknown error occurred during the search";
            } else {
                scope.error = response.data.message;
            }
        }
        
        scope.loading = false;
    });
};