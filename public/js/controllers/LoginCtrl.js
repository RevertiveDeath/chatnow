//  public/js/controllers/LoginCtrl.js

/* global angular */
angular.module('LoginCtrl', []).controller('LoginController', ['$scope', '$location', '$timeout', '$rootScope', '$http', function($scope, $location, $timeout, $rootScope, $http) {
    //Init scope
    $scope.credentials = {
        username: '',
        password: ''
    };

    $scope.registering = false;
    
    $location.url($location.path());
    
    //Check if session storage is active
    $rootScope.getData();
    
    if($rootScope.user != null && $rootScope.user != undefined) {
        $rootScope.loggedIn = true;
        $location.path("/");
    }
    
    //Login clicked
    $scope.login = function(credentials) {
        $scope.loginloading = true;
        $scope.loginerror = null;
                
        var username = credentials.username;
        var password = Hash(credentials.password);
        
        $http({
            method: 'POST',
            url: '/api/login',
            data: {
                "username": username,
                "password": password
            }
        }).then(function(response) {
            var data = response.data;
            var result = {
                successful: false,
                id: null,
                username: null,
                token: null
            }
            if(data.code == 200) {
                result.successful = true;
                result.id = data.user.id;
                result.username = data.user.username;
                result.token = data.user.token;
            }
            
            if(result.successful) {
                //Login successful, fill session and redirect to chat view
                $rootScope.setData({
                    id: result.id,
                    username: result.username,
                    token: result.token
                });
                $rootScope.loadNotifications();
                $rootScope.redirect = true;
                $location.path("/chats");
            } else {
                //Auth failed
                $scope.loginloading = false;
                $scope.loginerror = "Invalid username/email or password";
                return;
            }
        });
        
        
    }
    
    //Register clicked (Toggles registration form)
    $scope.toggleRegister = function() {
        $scope.registering = !$scope.registering;
        $scope.registererror = null;
        $scope.account = {
            username: '',
            email: '',
            password: '',
            password2: ''
        }
    }
    
    //Register account
    $scope.register = function(account) {
        $scope.registerloading = true;
        $scope.registererror = null;
        $scope.registerstatus = "Verifying input fields";
        
        if(account.username.trim().length == 0) {
            $scope.registererror = "Username cannot be empty";
        } else if(account.username.trim().length < 6) {
            $scope.registererror = "Username must be at least 6 characters long";
        } else  if( /[^a-zA-Z0-9]/.test(account.username.trim()) ) {
            $scope.registererror = "Username is not alphanumeric"
        } else if(account.email.trim().length == 0) {
            $scope.registererror = "Email cannot be empty";
        } else if(account.password.trim().length == 0) {
            $scope.registererror = "Password cannot be empty";
        } else if(account.password.trim().length < 8) {
            $scope.registererror = "Password must be at least 8 characters long";
        } else if(account.password.trim() != account.password2.trim()) {
            $scope.registererror = "Passwords do not match";
        }
        
        if($scope.registererror != null) {
            $scope.registerloading = false;
            $scope.registerstatus = null;
            return;
        }
        
        //Input fields valid - display inputted data
        var passdisplay = '';
        for(var i = 0; i < account.password.length; i++) {
            passdisplay += '*';
        }
        $scope.passdisplay = passdisplay;
        
        //Verify username is unique
        $scope.registerstatus = "Checking username uniqueness";
        $http({
            method: 'GET',
            url: '/api/verify',
            params: {
                username: account.username
            }
        }).then(function(response) {
            var result = response.data;
            if(result.code == 200 && result.user == null) {
                //Success - username is unique
                
                //Verify email is unique
                $scope.registerstatus = "Checking email uniqueness";
                
                $http({
                    method: 'GET',
                    url: '/api/verify',
                    params: {
                        email: account.email
                    }
                }).then(function(response) {
                    var result = response.data;
                    if(result.code == 200 && result.user == null) {
                        //Success - email is unique
                        
                        //Create the account
                        $scope.registerstatus = "All info valid. Creating your account";
                        
                        RegisterAccount($scope, $http, account, function(result) {
                            if(result.successful) {
                                //Account created, fill session and redirect to chat view
                                $rootScope.redirect = true;
                                $rootScope.setData({
                                    id: result.id,
                                    username: result.username,
                                    token: result.token
                                });
                                $location.path("/chats");
                            } else {
                                //Auth failed
                                $scope.registerstatus = null;
                                $scope.registerloading = false;
                                $scope.registererror = "Failed to create account. Please try again";
                                return;
                            }
                        });
                    } else { //Email is not unique
                        $scope.registererror = "Email already in use";
                        $scope.registerstatus = null;
                        $scope.registerloading = false;
                        return;
                    }
                });
            } else { //Username is not unique
                $scope.registererror = "Username already in use";
                $scope.registerstatus = null;
                $scope.registerloading = false;
                return;
            }
        });
    }
}]);

var RegisterAccount = function(scope, http, account, callback) {
    var username = account.username;
    var email = account.email;
    var password = Hash(account.password);
    
    http({
        method: 'POST',
        url: '/api/register',
        data: {
            "username": username,
            "email": email,
            "password": password
        }
    }).then(function(response) {
        var data = response.data;
        var result = {
            successful: false,
            id: null,
            username: null,
            token: null
        }
        if(data.code == 200) {
            result.successful = true;
            result.id = data.user.id;
            result.username = data.user.username;
            result.token = data.user.token;
        }
        
        //Return result to callback function
        callback(result);
    });
}

var Hash = function(data) {
    var hash = 0, i, chr, len;
    
    for (i = 0, len = data.length; i < len; i++) {
        chr   = data.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}