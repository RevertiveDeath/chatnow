//  public/js/app.js

/* global angular */
/* global moment */
// Inject services into app
(function() {
    //Create app with routing and controllers
    var app = angular.module('myApp', [
        'ngRoute',
        'ngStorage',
        'ngSanitize',
        'appRoutes',
        'MainCtrl',
        'ChatCtrl',
        'LoginCtrl',
        'ProfileCtrl',
        'SettingsCtrl',
        'SearchCtrl',
        'FriendCtrl'
    ]);
    
    //Global functions
    app.run(function($rootScope, $localStorage, $http, $location, $document, $window) {
        
        //Check for notifications
        $rootScope.loadNotifications = function() {
            $rootScope.notificationLoading = true;
            
            //Function to load user data for notification
            var LoadFriendRequest = function(rootScope, index) {
                $http({
                    method: 'GET',
                    url: '/api/profile',
                    params: {
                        "id": rootScope.notifications[index].sender
                    }
                }).then(function(response) {
                    if(index == rootScope.notifications.length - 1) {
                        $rootScope.notificationLoading = false;
                    }
                    if(response.data.code == 200) {
                        rootScope.notifications[index].user = response.data.user;
                    } else {
                        rootScope.notifications[index].message = response.data.message;
                    }
                });
            }
            
            $http({
                method: 'GET',
                url: '/api/request',
                params: {
                    "user": $rootScope.user.id,
                    "token": $rootScope.user.token,
                }
            }).then(function(response) {
                if(response.data.code == 200) {
                    $rootScope.notifications = response.data.requests;
                    if($rootScope.notifications.length > 0) {
                        for(var i = 0; i < $rootScope.notifications.length; i++) {
                            LoadFriendRequest($rootScope, i);
                        }
                    } else {
                        $rootScope.notifications = null;
                        $rootScope.notificationLoading = false; 
                    }
                } else {
                    $rootScope.notifications = [ { message: "Error loading notifications" } ];
                    $rootScope.notificationLoading = false;
                }
            });
        }
        
        //Set our global user variable and flush the data to local storage
        $rootScope.setData = function(user) {
            $rootScope.user = user;
            $rootScope.loggedIn = true;
            $localStorage.user = user;
        }
        
        //Reload all data
        $rootScope.reloadData = function() {
            if($rootScope != null) {
                $http({
                    method: 'GET',
                    url: '/api/login',
                    params: {
                        "user": $rootScope.user.id,
                        "token": $rootScope.user.token
                    }
                }).then(function(response) {
                    //200 is the only OK response
                    if(response.data.code != 200) {
                        //Data was bad, clear it
                        $rootScope.loggedIn = false;
                        $rootScope.user = null;
                        $localStorage.$reset();
                        //Stops FrontEnd and reroutes to login immediately
                        $location.path('/login');
                    } else {
                        /*
                            By reloading the id, username and token from the backend,
                            we remove the ability for users to change their username in localstorage.
                            
                            Although changing their username in localstorage is
                            pointless and does not actually do anything, this still
                            adds more extensibility for this function later.
                        */
                        $rootScope.user = response.data.user;
                        $localStorage.user = response.data.user;
                        //Load notifications
                        $rootScope.loadNotifications();
                    }
                });
            }
        }
        
        //Gets localstorage data and verifies it
        $rootScope.getData = function() {
            if(!$rootScope.loggedIn || $rootScope.user == null) {
                if($localStorage.user != null) {
                    //Assume data is valid initially (So FrontEnd doesn't hang)
                    $rootScope.loggedIn = true;
                    $rootScope.user = $localStorage.user;
                    //Verify localstorage data is valid
                    $http({
                        method: 'GET',
                        url: '/api/login',
                        params: {
                            "user": $rootScope.user.id,
                            "token": $rootScope.user.token
                        }
                    }).then(function(response) {
                        //200 is the only OK response
                        if(response.data.code != 200) {
                            //Data was bad, clear it
                            $rootScope.loggedIn = false;
                            $rootScope.user = null;
                            $localStorage.$reset();
                            //Stops FrontEnd and reroutes to login immediately
                            $location.path('/login');
                        } else {
                            /*
                                By reloading the id, username and token from the backend,
                                we remove the ability for users to change their username in localstorage.
                                
                                Although changing their username in localstorage is
                                pointless and does not actually do anything, this still
                                adds more extensibility for this function later.
                            */
                            $rootScope.user = response.data.user;
                            $localStorage.user = response.data.user;
                            //Load notifications
                            $rootScope.loadNotifications();
                        }
                    });
                }
            }
        }
        
        //Search for a user
        $rootScope.search = function(searchparam) {
            if(searchparam !== undefined && searchparam !== null && searchparam.trim().length > 0) {
                $location.path('/search').search({param: searchparam});
            } else {
                $location.search()['param'] = "";
                $location.path('/search');
            }
            $rootScope.searchparam = undefined;
        }
        
        //Logout
        $rootScope.logout = function() {
            $rootScope.user = null;
            $rootScope.loggedIn = false;
            //Clear session storage
            $localStorage.$reset();
        }
        
        //Retrieve logged in username
        $rootScope.username = function() {
            if($rootScope.user == undefined ||
                $rootScope.user == null ||
                $rootScope.user.username == undefined || 
                $rootScope.user.username == null)
            {
                return "";
            }
            return $rootScope.user.username;
        }
       
       //Forces us to run on a secure protocol
        var forceSSL = function () {
            if ($location.protocol() !== 'https') {
                $window.location.href = $location.absUrl().replace('http', 'https');
            }
        };
        forceSSL();
    });
    
    //DateTime formatter
    app.filter("formatDateTime", function() {
        return function(timestamp) {
            return moment(timestamp).format('h:mm a - MMM DD, YYYY');
        }
    });
    
    //Enter press directive
    app.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });
     
                    event.preventDefault();
                }
            });
        };
    });

    //HTML5 Mode
    app.config( ['$locationProvider', function($locationProvider) {
        $locationProvider.html5Mode (true);
    }]);
})();