//  server.js

// modules
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var path = require('path');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

// configuration
var db = require('./config/db');
var version = "Alpha - v1.1";
var port = process.env.PORT || 8080; 
var mailTransport = nodemailer.createTransport(smtpTransport({
    host: 'smtp.gmail.com',
    secureConnection: true,
    port: 465,
    auth: {
        user: 'revertivedeath@gmail.com',
        pass: 'DanBailey1'
    }
}));

//Sends email based on ids
var MailFriendRequest = function(receiver, sender, type, models) {
    //First resolve receiver and sender objects
    models.User.findById(receiver, function(err, r) {
        if(!err && r != undefined && r != null) {
            models.User.findById(sender, function(err, s) {
                if(!err && s != undefined && s != null) {
                    if(type == 'friend') {
                        var senderName = s.username;
                        if(s.fullname !== null && s.fullname !== undefined && s.fullname.length > 0) {
                            senderName = s.fullname + " (" + s.username + ")";
                        }
                        var emailOptions = {
                            to: r.email,
                            from: '"ChatNow" <revertivedeath@gmail.com>',
                            subject: 'Friend request from ' + senderName + '!',
                            text: 'You have received a friend request from ' + senderName + ' head over to ChatNow to view it!',
                            html: '<h3>New Friend Request</h3><p><b>' + senderName + "</b> would like to add you as a friend!</p><br /><a href='chatnow-revertivedeath.c9users.io/profile?user=" + s.username + "'>View Now</a>"
                        };
                        mailTransport.sendMail(emailOptions);
                    }
                } 
            });
        } 
    });
}

var MailChatRequest = function(receiver, sender, chat, models) {
    //First resolve receiver and sender objects
    models.User.findById(receiver, function(err, r) {
        if(!err && r != undefined && r != null) {
            models.User.findById(sender, function(err, s) {
                if(!err && s != undefined && s != null) {
                    var senderName = s.username;
                    if(s.fullname !== null && s.fullname !== undefined && s.fullname.length > 0) {
                        senderName = s.fullname + " (" + s.username + ")";
                    }
                    var emailOptions = {
                        to: r.email,
                        from: '"ChatNow" <revertivedeath@gmail.com>',
                        subject: senderName + ' has added you to a chat!',
                        text: 'You have been added to a chat by ' + senderName + 'Head over to ChatNow to view it!',
                        html: '<h3>New Chat Invite</h3><p><b>' + senderName + "</b> has invited you to their new chat: " + chat.name + "!</p><br /><a href='chatnow-revertivedeath.c9users.io/chats'>View Chats</a>"
                    };
                    mailTransport.sendMail(emailOptions);
                } 
            });
        } 
    });
}

// Connect to DB
console.log('Connect to database (' + db.url + ')');
mongoose.connect(db.url);
console.log('Success');

// authentication
var auth = require('./app/auth.js');

// init app
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(express.static(__dirname + '/public'));

// MODELS
var models = require('./app/models');

// ROUTING
var router = express.Router();

// Middleware for all requests
router.use(function(req, res, next) {
    //Check if access token is required and if so, validate it
    var authstatus = auth.authenticate(req, models, function(result) {
        //Log status of auth
        console.log('[' + req.method + '] ' + req.originalUrl);
        console.log('    AUTH STATUS: ' + result.code.toString());
        
        if(result.success == true) {
            next();
        } else {
            res.json({
                message: result.message,
                code: result.code
            });
        }
        return;
    });
    
    if(authstatus.code != 1) //Not waiting on callback
    {
        //Log status of request
        console.log('[' + req.method + '] ' + req.originalUrl);
        console.log('    AUTH STATUS: ' + authstatus.code.toString());
        
        if(authstatus.success) {
            next();
        } else if(req.originalUrl === '/api/token') {
            res.json({
                message: 'TEST Auth Token',
                token: auth.generateToken()
            });
        } else {
            res.json({
                message: authstatus.message,
                code: authstatus.code
            });
        }
    }
});

// MODEL REQUESTS
//Users
router.route('/login')
    //Verify user session data is valid
    .get(function(req, res) {
        var id = req.query.user;
        var token = req.query.token;
        
        models.User.findOne({ '_id': id, 'token': token }, function(err, user) {
            if(err || user == null) {
                res.json({
                    code: 104
                });
            } else {
                res.json({
                    code: 200,
                    message: "OK",
                    user: {
                        id: user.id,
                        username: user.username,
                        token: user.token,
                        audio: user.audio
                    }
                });
            }
        });
        
    })
    // Login an existing user
    .post(function(req, res) {
        var username = req.body.username;
        var password = req.body.password;
        var token = auth.generateToken();
        
        var userdata = {
            username: null,
            joined: null,
            token: token,
            id: null
        };
        
        //Authenticate the user
        models.User.findOneAndUpdate({ 'username': username, 'password': password }, {$set:{'token': token}}, {new: true}, function(err, user) {
            if(err) {
                res.json({
                    code: 104
                });
            } else if (user == undefined || user == null) {
                //Try again using email
                models.User.findOneAndUpdate({ 'email': username, 'password': password }, {$set:{'token': token}}, {new: true}, function(err, user) {
                    if(err || user == null) {
                        res.json({
                            code: 104
                        });
                    } else {
                        userdata.username = user.username;
                        userdata.joined = user.joined;
                        userdata.email = user.email;
                        userdata.token = user.token;
                        userdata.id = user.id;
                        
                        res.json({
                            code: 200,
                            user: userdata
                        });
                    }
                });
            } else {
                userdata.username = user.username;
                userdata.joined = user.joined;
                userdata.email = user.email;
                userdata.token = user.token;
                userdata.id = user.id;
                userdata.audio = user.audio;
                
                res.json({
                    code: 200,
                    user: userdata
                });
            }
        });
    });

router.route('/register')
    //Update a user's password
    .put(function(req, res) {
        var id = req.body.user;
        var oldpass = req.body.oldpass;
        var pass = req.body.pass;
        
        models.User.findOneAndUpdate({ '_id': id, 'password': oldpass}, {$set:{'password': pass}}, {new: true}, function(err, user) {
            if(err) {
                res.json({
                    code: 400,
                    message: "An error has occurred"
                });
            } else if(user === undefined || user === null) {
                res.json({
                    code: 402,
                    message: "Incorrect password"
                });
            } else {
                res.json({
                    code: 200,
                    message: "Success"
                });
            }
        });
    })
    // Register a new user
    .post(function(req, res) {
        var timestamp = Date.now();
        //Create the new user
        var user = new models.User();
        user.username = req.body.username;
        user.email = req.body.email;
        user.joined = timestamp;
        user.chats = 0;
        user.messages = 0;
        user.audio = true;
        user.password = req.body.password;
        user.token = auth.generateToken();
        user.friends = []; //Empty friend array
        
        var userdata = {
            username: user.username,
            joined: user.joined,
            email: user.email,
            token: user.token,
            id: null
        };
        
        user.save(function(err) {
            if(err) {
                res.send(err);
            } else {
                userdata.id = user.id;
                res.json({ 
                    code: 200,
                    user: userdata
                });
            }
        });
    });
    
//Verify data
router.route('/verify')
    // Verify certain data
    .get(function(req, res) {
        var username = req.query.username;
        var email = req.query.email;
        
        if(username != undefined && username != null) {
            var uquery = models.User.findOne({ 'username': username });
            uquery.exec(function(err, user) {
                if(err) {
                    res.json({
                        message: "ERROR",
                        code: 410
                    });
                } else {
                    res.json({
                        message: "OK",
                        code: 200,
                        user: user
                    });
                }
            });
        } else if(email != undefined && email != null) {
            var equery = models.User.findOne({ 'email': email });
            equery.exec(function(err, user) {
                if(err) {
                    res.json({
                        message: "ERROR",
                        code: 410
                    });
                } else {
                    res.json({
                        message: "OK",
                        code: 200,
                        user: user
                    });
                }
            });
        } else {
            res.json({
                message: "Nothing to verify",
                code: 444
            })
        }
    });
    
//User profile
router.route('/profile')
    //Gets basic user profile info (No auth required)
    .get(function(req, res) {
        //This way we can get a profile via id or username
        var query;
        var username = req.query.user;
        var id = req.query.id;
        if(username !== undefined && username !== null) {
            query = { username: username };
        } else {
            query = { '_id': id };
        }
        
        //Query db
        models.User.findOne(query, function(err, user) {
            if(err) {
                res.json({
                    message: "ERROR",
                    code: 410
                });
            } else if(user == null) {
                res.json({
                    message: "User not found",
                    code: 404
                });
            } else {
                res.json({
                    message: "OK",
                    code: 200,
                    user: {
                        id: user.id,
                        username: user.username,
                        fullname: user.fullname,
                        joined: user.joined,
                        email: user.email,
                        chats: user.chats,
                        messages: user.messages,
                        color: user.color,
                        friends: user.friends
                    }
                });
            }
        });
    })
    
    //Update user's profile data
    .post(function(req, res) {
        var user = req.body;
        var update = null;
        if(user.fullname != null && user.color != null) {
            update = {
                $set:{
                    'fullname': user.fullname, 
                    'color': user.color
                }};
        } else if(user.audio != null) {
            update = {
                $set:{
                    'audio' : user.audio
                }};
        }
        if(update == null) {
            res.json({
                code: 400,
                message: "NOT OK"
            });
        } else {
            models.User.findOneAndUpdate({ '_id': user.user}, update, {new: true}, function(err, user) {
                if(err || user === undefined || user === null) {
                    res.json({
                        code: 400,
                        message: "NOT OK"
                    });
                } else {
                    if(user.audio != null) {
                        res.json({
                            code: 200,
                            message: "OK",
                            audio: user.audio
                        });
                    } else {
                        res.json({
                            code: 200,
                            message: "OK"
                        });
                    }
                }
            });
        }
    })
    
    //Delete a user's account
    .delete(function(req, res) {
        var id = req.query.user;
        models.User.findById(id, function(err, user) {
            if(err) {
                res.json({
                    message: "An error has occurred",
                    code: 400
                });
            } else {
                //Remove this account from all friend's friend lists
                for(var i = 0; i < user.friends.length; i++) {
                    models.User.findById(user.friends[i], function(err, friend) {
                        if(!err) {
                            var index = friend.friends.indexOf(id);
                            friend.friends.splice(index, 1);
                            friend.save(function(err) { 
                                if(err) {
                                    //Even if an error occurs we don't care
                                    //The user is deleted anyways
                                }
                            });
                        }
                    });
                }
                //Remove the account from the database
                models.User.remove({'_id': id}, function (err) {
                    if(err) {
                        res.json({
                            message: "An error has occurred",
                            code: 400
                        });
                    } else {
                        res.json({
                            message: "Success",
                            code: 200
                        });
                    }
                });
            }
        });
    });
    
//Searches
router.route('/search')
    //Gets search results
    .get(function(req, res) {
        /* Building the query:
            I have two types of query: fullname and username
            I join these with an outer $or clause
        */
        var query = { $or: [] };
        /*
            and1 represents the fullname query,
            and2 represents the username query
        */
        var and1 = { $and: []};
        var and2 = { $and: []};
        //Load the parameter array from the request
        var params = req.query.params;
        //If the param is a string, we convert to array format
        if(params.constructor !== Array) {
            params = [ params ];
        }
        //Now we dynamically create the query
        for(var i = 0; i < params.length; i++) {
            //Each fullname portion must match a parameter
            and1.$and.push(
                {
                    fullname: new RegExp(params[i], "i")
                }
            );
            //Or each username portion must match a parameter
            and2.$and.push(
                {
                    username: new RegExp(params[i], "i")
                }
            );
        }
        //Now we add both clauses to the outer or clause
        query.$or.push(and1);
        query.$or.push(and2);
        //Execute the query and sort the results
        models.User.find(query).select('username fullname color').limit(40).sort('field fullname').sort('field username').exec(function(err, results) {
            //An error occurred
            if(err) {
                res.json({
                    message: "An error has occurred",
                    code: 400
                });
                //No users matched the parameters
            } else if(results == undefined || results == null || results.length == 0) {
                res.json({
                    message: "No users found",
                    code: 404
                });
                //We have the data, return it
            } else {
                res.json({
                    message: "Success",
                    code: 200,
                    results: results
                });
            }
        });
    });
        
//Requests
router.route('/request')
    //Check for requests given search params
    .get(function(req, res) {
        //Request data
        var sender = req.query.sender;
        if(sender == null || sender == undefined) {
            //In this case we return all requests for current user
            var id = req.query.user;
            models.Request.find({receivers: id}, function(err, requests) {
                if(err) {
                    res.json({
                        code: 400,
                        message: "Error retrieving requests"
                    });
                } else {
                    if(requests == undefined || requests == null) {
                        requests = [];
                    }
                    res.json({
                        code: 200,
                        message: "Success",
                        requests: requests
                    });
                }
            });
        } else {
            //Return specific request data
            var type = req.query.type;
            var receivers = [];
            if(type == 'friend') {
                //Friend request, find ID of friend to push onto receivers
                receivers.push(req.query.friend);
                models.Request.findOne({
                    type: type,
                    sender: sender,
                    receivers: receivers
                }).exec(function(err, request) {
                    if(err) {
                        res.json({
                            code: 400,
                            message: "Error fetching request data"
                        });
                    } else {
                        res.json({
                            code: 200,
                            message: "Success",
                            requested: (request != undefined && request != null)
                        });
                    }
                });
            } else {
                res.json({
                    code: 400,
                    message: "Unimplemented Exception"
                });
            }
        }
    })
    //Create a new request
    .post(function(req, res) {
        var sender = req.body.sender; //id of sender
        //Since chat requests may have multiple receivers we make receiver an array
        var receivers = [];
        if(req.body.receivers.constructor !== Array) {
            receivers.push(req.body.receivers); //Single user
        } else {
            receivers = req.body.receivers; //Already an array
        }
        var date = Date.now();
        var type = req.body.type;
        
        var request = new models.Request();
        
        request.type = type;
        request.date = date;
        request.sender = sender;
        request.receivers = receivers;

        //We have all request data, make sure this request doesn't exist yet
        models.Request.findOne({
            type: type,
            sender: sender,
            receivers: receivers
        }).select('id').exec(function(err, existing) {
            if(err) {
                res.json({
                    code: 400,
                    message: "Error checking for duplicate request"
                });
            } else if(existing != undefined || existing != null) {
                res.json({
                    code: 400,
                    message: "Request already sent"
                });
            } else {
                request.save(function(err) {
                    for(var i = 0; i < receivers.length; i++) {
                        MailFriendRequest(
                            receivers[i], 
                            sender,
                            type,
                            models);
                    }
                    if(err) {
                        res.json({
                            code: 400,
                            message: "Error sending request"
                        });
                    } else {
                        res.json({ 
                            code: 200,
                            message: "Success"
                        });
                    }
                });
            }
        });
    })
    //Remove request
    .delete(function(req, res) {
        //Request data
        var sender = req.query.sender;
        var type = req.query.type;
        var accept = req.query.accept;
        var receivers = [];
        if(type == 'friend') {
            //Friend request
            var friend = req.query.friend;
            //We are deleting because the user accepted the request
            //This means we add to both friend lists and then delete
            if(accept == 'true') {
                models.User.findById(sender, function(err, user1) {
                    if(err || user1 == undefined || user1 == null) {
                        res.json({
                            code: 400,
                            message: "Error accepting request"
                        });
                    } else {
                        user1.friends.push(friend);
                        user1.save(function(err) {
                            if(err) {
                                res.json({
                                    code: 400,
                                    message: "Error accepting request"
                                });
                            } else {
                                models.User.findById(friend, function(err, user2) {
                                    if(err || user2 == undefined || user2 == null) {
                                        res.json({
                                            code: 400,
                                            message: "Error accepting request"
                                        });
                                    } else {
                                        user2.friends.push(sender);
                                        user2.save(function(err) {
                                            if(err) {
                                                res.json({
                                                    code: 400,
                                                    message: "Error accepting request"
                                                });
                                            } else {
                                                //Removes both requests if both users requested each other's friendship
                                                models.Request.remove({ $or: [
                                                    {
                                                        type: type,
                                                        sender: sender,
                                                        receivers: [ friend ]
                                                    }, {
                                                        type: type,
                                                        sender: friend,
                                                        receivers: [ sender ]
                                                    }
                                                ]}).exec(function(err) {
                                                    if(err) {
                                                        res.json({
                                                            code: 400,
                                                            message: "Error cancelling request"
                                                        });
                                                    } else {
                                                        res.json({
                                                            code: 200,
                                                            message: "Success"
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                //Removes both requests if both users requested each other's friendship
                models.Request.remove({ $or: [
                    {
                        type: type,
                        sender: sender,
                        receivers: [ friend ]
                    }, {
                        type: type,
                        sender: friend,
                        receivers: [ sender ]
                    }
                ]}).exec(function(err) {
                    if(err) {
                        res.json({
                            code: 400,
                            message: "Error cancelling request"
                        });
                    } else {
                        res.json({
                            code: 200,
                            message: "Success"
                        });
                    }
                });
            }
        } else {
            res.json({
                code: 400,
                message: "Unimplemented Exception"
            });
        }
    });
    
//Friendship status
router.route('/friend')
    //Get friendship status (true or false)
    .get(function(req, res) {
        var id = req.query.user;
        var friend = req.query.friend;
        models.User.findById(id, function(err, user) {
            if(err) {
                res.json({
                    code: 400,
                    message: "Failed to check friendship"
                });
            } else if(user == undefined || user == null) {
                res.json({
                    code: 404,
                    message: "User not found"
                });
            } else {
                res.json({
                    code: 200,
                    message: "Success",
                    friends: (user.friends.indexOf(friend) > -1)
                });
            }
        });
    })
    //Remove a friend
    .delete(function(req, res) {
        var id = req.query.user;
        var friend = req.query.friend;
        models.User.findById(id, function(err, user1) {
            if(err) {
                res.json({
                    code: 400,
                    message: "Failed to remove friend"
                });
            } else if(user1 == undefined || user1 == null || user1.friends.indexOf(friend) < 0) {
                res.json({
                    code: 404,
                    message: "User not found"
                });
            } else {
                var index = user1.friends.indexOf(friend);
                user1.friends.splice(index, 1);
                user1.save(function(err) {
                    if(err) {
                        res.json({
                            code: 400,
                            message: "Failed to remove friend"
                        });
                    } else {
                        models.User.findById(friend, function(err, user2) {
                            if(err) {
                                res.json({
                                    code: 400,
                                    message: "Failed to remove friend"
                                });
                            } else if(user2 == undefined || user2 == null || user2.friends.indexOf(id) < 0) {
                                res.json({
                                    code: 404,
                                    message: "User not found"
                                });
                            } else {
                                var index2 = user2.friends.indexOf(id);
                                user2.friends.splice(index2, 1);
                                user2.save(function(err) {
                                    if(err) {
                                        res.json({
                                            code: 400,
                                            message: "Failed to remove friend"
                                        });
                                    } else {
                                        res.json({
                                            code: 200,
                                            message: "Success",
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    });

//Statistics
router.route('/global')
    // Returns all global stats
    .get(function(req, res) {
        models.User.count(function(err, usercount) {
            if(err) {
                res.send(err);
            } else {
                models.Chat.count(function(err, chatcount) {
                    if(err) {
                        res.send(err);
                    } else {
                        models.Message.count(function(err, msgcount) {
                            if(err) {
                                res.send(err);
                            } else {
                                res.json({
                                    message: 'Successfully retrieved',
                                    chats: chatcount,
                                    messages: msgcount,
                                    users: usercount,
                                    version: version
                                });
                            }
                        });
                    }
                });
            }
        });
    });

router.route('/chats')
    // Retrieve all chats
    .get(function(req, res) {
        var userid = req.query.user;
        models.Chat.find({ users: userid }).sort('field -update').exec(function(err, chats) {
            if(err) {
                res.json({
                    code: 400,
                    message: "Error retrieving chats"
                });
            } else {
                res.json( { 
                    message: 'Successfully retrieved',
                    code: 200,
                    chats: chats
                });
            }
        });
    })

    // Create a chat
    .post(function(req, res) {
        var sender = req.body.user;
        var requsers = req.body.users;
        var users = [];
        //Create new instance of chat model
        var chat = new models.Chat();
        //Set chat name from request
        var timestamp = Date.now();
        chat.date = timestamp;
        chat.update = timestamp;
        chat.users = requsers; //Includes creator
        chat.name = req.body.name;
        
        //Validate name/subject
        if(chat.name === null || chat.name === undefined || chat.name.length == 0) {
            res.json({
                code: 405,
                message: "Chat subject cannot be empty"
            });
            return;
        } else if(chat.name.length > 100) {
            res.json({
                code: 406,
                message: "Chat subject must be less than 100 characters"
            });
            return;
        }
        
        //Verify users and create chat
        var usercount = 0;
        var error = false;
        var status = 200;
        var responded = false;
        for(var i = 0; i < requsers.length; i++) {
            models.User.findById(requsers[i], function(err, user) {
                if(err) {
                    if(!error) {
                        status = 400;
                        error = true;
                    }
                } else if(user == null) {
                    if(!error) {
                        status = 404;
                        error = true;
                    }
                }
                
                if(error && !responded) {
                    if(status == 404) {
                        res.json({
                            code: 404,
                            message: "User not found"
                        });
                        responded == true;
                    } else {
                        res.json({
                            code: 400,
                            message: "Error fetching user data"
                        });
                        responded == true;
                    }
                } else {
                    //No error
                    usercount++;
                    users.push(user);
                    //If this is the user we want to update their chat count
                    if(user.id == sender) {
                        if(user.chats !== null && user.chats !== undefined) {
                            user.chats++;
                        } else {
                            user.chats = 1;
                        }
                        user.save(function(err) {
                             if(err) {
                                 //Really, there isn't much to do here
                             }
                        });
                    }
                }
                    
                //Create chat if all users verified
                if(usercount == requsers.length && !responded) {
                    chat.save(function(err) {
                        if(err) {
                            res.json({
                                message: "Error creating chat",
                                code: 410
                            });
                            return;
                        } else {
                            //Mail request
                            for(var x = 0; x < chat.users.length; x++) {
                                if(chat.users[x] !== sender) {
                                    MailChatRequest(chat.users[x], sender, chat, models);
                                }
                            }
                            res.json({
                               message: "Chat created",
                               code: 200,
                               chat: chat
                            });
                            return;
                        }
                    });
                }
            });
        }
        
    });
    
router.route('/chats/:chat_id')
    // Get chat with the id provided
    .get(function(req, res) {
        models.Chat.findById(req.params.chat_id, function(err, chat) {
           if(err) {
               res.send(err);
           } else {
               res.json(chat);
           }
        });
    })
    
    // Update chat
    .put(function(req, res) {
        var timestamp = Date.now();
        models.Chat.findById(req.params.chat_id, function(err, chat) {
            if(err) {
                res.send(err);
            } else {
                chat.name = req.body.name; // Update info
                chat.update = timestamp;
                chat.save(function(err) {
                    if(err) {
                        res.send(err);
                    } else {
                        res.json({ 
                            message: 'Chat updated',
                            chat: chat
                        });
                    }
                });
            }
        });
    })
    
    // Delete chat (Or delete user from chat if there are other participants)
    .delete(function(req, res) {
        //Store user id
        var user = req.query.user;
        //First get the chat
        models.Chat.findById(req.params.chat_id, function(err, chatdata) {
            if(err || chatdata == null || chatdata == undefined || chatdata.users == null || chatdata.users == undefined) {
                res.json({
                    code: 404,
                    message: "Could not retrieve chat data"
                });
            } else {
                //We have the chat
                if(chatdata.users.length > 1) {
                    //Just remove the user from the chat and update
                    var index = -1;
                    for(var i = 0; i < chatdata.users.length; i++) {
                        if(chatdata.users[i] == user) {
                            index = i;
                            break;
                        }
                    }
                    if(index > -1) {
                        chatdata.users.splice(index, 1);
                        chatdata.save(function(err) {
                            if(err) {
                                res.json({
                                   message: 'Failed to update chat',
                                   code: 400
                                });
                            } else {
                                res.json({ 
                                    code: 200,
                                    message: 'User removed from chat'
                                });
                            }
                        });
                    } else {
                        res.json({
                            code: 400,
                            message: "User not found in chat users"
                        });
                    }
                } else {
                    //Delete the chat
                    models.Chat.remove({
                        _id: req.params.chat_id
                    }, function(err, chat) {
                        if(err) {
                            res.json({
                                code: 400,
                                message: 'Failed to delete chat'
                            });
                        } else {
                            models.Message.remove({
                                chat: req.params.chat_id
                            }, function(err, messages) {
                                if(err) {
                                    res.json({
                                        code: 400,
                                        message: 'Failed to delete chat messages'
                                    });
                                } else {
                                    res.json({ 
                                        code: 200,
                                        message: 'Chat deleted'
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    });

//TODO remove test route
router.route('/messages')
    // Retrieve ALL messages
    .get(function(req, res) {
        models.Message.find({}).sort('field chat').sort('field date').exec(
            function(err, messages) {
                if(err) {
                    res.send(err);
                } else {
                    res.json( { 
                        message: 'Successfully retrieved',
                        messages: messages
                    });
                }
            });
    });

router.route('/messages/:chat_id')
    // Retrieve all messages for a given chat
    .get(function(req, res) {
        models.Message.find({
            chat: req.params.chat_id
        }).limit(100).sort('field -date').exec(function(err, messages) {
            if(err || messages == null || messages == undefined || messages.length == null || messages.length == undefined) {
                res.json({
                    code: 400,
                    message: "Error retrieving messages"
                });
            } else {
                res.json( { 
                    code: 200,
                    message: 'Successfully retrieved',
                    messages: messages
                });
            }
        });
    })

    // Create a message
    .post(function(req, res) {
        var sender = req.body.user;
        var content = req.body.content;
        var statusMessage = req.body.statusMessage;
        if(statusMessage == null || statusMessage == undefined) {
            statusMessage = false;
        }
        if(content.length > 300) {
            content = content.substring(0, 297) + "...";
        }
        var chatid = req.params.chat_id;
        var timestamp = Date.now();
        //Create the new message
        var msg = new models.Message();
        msg.content = content;
        msg.sender = sender;
        msg.date = timestamp;
        msg.statusMessage = statusMessage;
        
        models.Chat.findById(chatid, function(err, chat) {
            if(err) {
                res.send(err);
            } else {
                chat.update = timestamp;
                chat.save(function(err) {
                    if(err) {
                        res.send(err);
                    } else {
                        msg.chat = chat;
                        msg.save(function(err) {
                            if(err) {
                                res.send(err);
                            } else {
                                //We use responded because even if this fails the message still was created
                                var responded = false;
                                //Now we save the user's new message count
                                models.User.findById(sender, function(err, user) {
                                    if(!err && user !== null && user !== undefined) {
                                        if(user.messages == null || user.messages == undefined) {
                                            user.messages = 1;
                                        } else {
                                            user.messages++;
                                        }
                                        user.save(function(err) {
                                           if(err) {
                                               //Not much we can do if this fails
                                           } 
                                        });
                                    }
                                });
                                if(!responded) {
                                    responded = true;
                                    res.json({ 
                                        code: 200,
                                        message: 'Message created',
                                        msg: msg
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });
    });
    
//TODO remove test route
router.route('/messages/:message_id')
    // Get message with the id provided
    .get(function(req, res) {
        models.Message.findById(req.params.message_id, function(err, msg) {
           if(err) {
               res.send(err);
           } else {
               res.json({
                   message: 'Successfully retrieved',
                   msg: msg
               });
           }
        });
    })
    
    // Update message
    .put(function(req, res) {
        var timestamp = Date.now();
        models.Message.findById(req.params.message_id, function(err, msg) {
            if(err) {
                res.send(err);
            } else {
                msg.content = req.body.content; // Update info
                msg.update = timestamp;
                if(req.body.chat !== null && req.body.chat !== undefined) {
                    msg.chat = req.body.chat;
                }
                
                models.Chat.findById(msg.chat, function(err, chat) {
                    if(err) {
                        res.send(err);
                    } else {
                        chat.update = timestamp;
                        chat.save(function(err) {
                            if(err) {
                                res.send(err);
                            } else {
                                msg.save(function(err) {
                                    if(err) {
                                        res.send(err);
                                    } else {
                                        res.json({ 
                                            message: 'Message updated',
                                            msg: msg
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    })
    
    // Delete message
    .delete(function(req, res) {
        models.Message.remove({
            _id: req.params.message_id
        }, function(err, msg) {
            if(err) {
                res.send(err);
            } else {
                res.json({ 
                    message: 'Message deleted'
                });
            }
        });
    });

// Server Routes (API CAllS)
router.get('/', function(req, res) {
    res.json({message: 'Chat Now API operational'}); 
});

app.use('/api', router);

// Route to handle all angular requests (Wildcard redirect)
app.get('*', function(req, res) {
    res.sendFile(path.resolve('public/index.html')); //Load public index.html file 
});

// START APPLICATION
app.listen(port);

// notify user that app is running
console.log('Server running on port ' + port);

// expose app
exports = module.exports = app;



/* NOTES
    db.users.find({"fullname": /bail/i})
*/